'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PriceSchema = new Schema({

	symbol_id: {
		type: String,
		default: ''
	},
	time_exchange: {
		type: Date,
        default: Date.now()
	},
	time_coinapi: {
        type: Date,
        default: Date.now()
	},
	uuid: {
		type: String,
		default: ''
	},
	price: {
		type: String,
		default: ''
	},
	size: {
		
		type: String,
		default:''
	},
	taker_side: {
        type: Date,
        default: Date.now
    }
});

var PriceModelSchema = mongoose.model('price', PriceSchema);
module.exports = PriceModelSchema