'use strict';

const paymentCtrl = require('../controllers/payment.controller');
import { Router } from 'express';
const router = new Router();

router.route('/payments/getAllPayments').get(paymentCtrl.getAllPayments);
router.route('/payments/getAllTransactions').get(paymentCtrl.getAllTransactions);
router.route('/payments/getAllVoters').get(paymentCtrl.getAllVoters);
router.route('/payments/getAllTweets').get(paymentCtrl.getAllTweets);
router.route('/payments/getBlockHeight').get(paymentCtrl.getBlockHeight);
router.route('/payments/sendMailContacts').post(paymentCtrl.sendMailContacts);
router.route('/payments/getAllPrices').get(paymentCtrl.getAllPrices);
router.route('/payments/getAllSymbols').get(paymentCtrl.getAllSymbols);
router.route('/payments/saveAllPrices').post(paymentCtrl.SaveAllPrices);


export default router;