'use strict';
require('../models/price');

const async = require('async');
const _ = require('lodash');
const axios = require('axios');
const Twitter = require('twitter');
const sgMail = require('@sendgrid/mail');
var mongoose = require('mongoose'), 
    Price = mongoose.model('price');



export async function getAllPayments(req,res) {
    
   
    
    const config = {
        method: 'get',
        url: 'https://node1.arknet.cloud/api/delegates/get?username=del',
        nethash: '6e84d08bd299ed97c212c886c98a57e36545c8f5d645ca7eeae63a8bd62d8988', // replace with your ark nethash
        version: '1.0.1',
        port: 4001,
        responseType: 'json',
    };
    axios(config).then((response) => {
        
                    res.status(200).json(response.data);
                })
                .catch((err) => {
                    res.status(400).json(err);
                });
 

}

export  function getAllTransactions(req, res) {
    const config = {
        method: 'get',
        url: `https://node1.arknet.cloud/api/transactions`,
        nethash: '6e84d08bd299ed97c212c886c98a57e36545c8f5d645ca7eeae63a8bd62d8988', // replace with your ark nethash
        version: '1.0.1',
        port: 4001,
        responseType: 'json',
    };
    axios(config).then((response) => {
            
                    res.status(200).json(response.data);
                })
                .catch((err) => {
                    res.status(400).json(err);
                });

}

export function getAllVoters (req, res) {
    const config = {
        method: 'get',
        url: `https://node1.arknet.cloud/api/delegates/voters?publicKey=03d9ed6e7f29daf12ef925d4ce5753aade23c8cfd52a0427240fb30ad6ec232fed`,
        nethash: '6e84d08bd299ed97c212c886c98a57e36545c8f5d645ca7eeae63a8bd62d8988', // replace with your ark nethash
        version: '1.0.1',
        port: 4001,
        responseType: 'json',
    };
    axios(config).then((response) => {
                 
                    res.status(200).json(response.data);
                })
                .catch((err) => {
                    res.status(400).json(err);
                });

}

export function getAllTweets (req, res) {
    const client = new Twitter({
        consumer_key: 'RHs9NFnKdzNDIJCbGTqIO2Qn5',
        consumer_secret: 'O9D4WIRyOSz3sDxzioOtl2TKyNrTtoRzENqxIYz0t3DPn6cgFq',
        access_token_key: '975831423981641733-043KyeGm2RuNot8K3A3bNWoqmz8bnR1',
        access_token_secret: 'mGZgnsQD9lDEA86QuRnNNihd9tGHIdEDkSKx0ff50p508'
        
      });

    client.get('statuses/user_timeline', { screen_name: 'Delegate_Del', count: 20 }, function(error, tweets, response) {
        if (!error) {

            res.status(200).json({ tweets: tweets });
        }
        else {
  
            es.status(500).json({ error: error });
        }
    });

}

export function getBlockHeight(req, res) {
    const config = {
        method: 'get',
        url: 'https://node1.arknet.cloud/api/blocks/getHeight',
        nethash: '6e84d08bd299ed97c212c886c98a57e36545c8f5d645ca7eeae63a8bd62d8988', // replace with your ark nethash
        version: '1.0.1',
        port: 4001,
        responseType: 'json',
    };
    axios(config).then((response) => {
        
                    res.status(200).json(response.data);
                })
                .catch((err) => {
                    res.status(400).json(err);
                });

}

export function sendMailContacts(req, res) {
    
    sgMail.setApiKey("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");
    const msg = {
        to: 'ark.delegate@gmail.com',
        from: 'sun0dragon@gmail.com',
        subject: 'Arkdel Message from: Ark Address',
        text: 'arkDel message',
        html:  `<p>Name:${req.body.name}</p><p>Ark Address:${req.body.address}</p><p>Description of issue:${req.body.description}</p>`,
    };
    let responseEmail = sgMail.send(msg);
    res.status(200).json({success: true});
}


export function getAllPrices( req, res) {
    Price.find({}, function(err, documents){
        if (err) {
            res.status(400).json(err);
        }
        res.status(200).json(documents);
    });
}

export function getAllSymbols( req, res) {
    const options = {

        headers: {'X-CoinAPI-Key': 'CE923B9F-B8E2-4036-871B-0902E15CEB30'},
        method: 'get',
        url: "https://rest.coinapi.io/v1/symbols?filter_symbol_id=ARK"
    };
    axios(options).then((response) => {
        console.log('response',response.data);
        res.status(200).json(response.data);
    })
    .catch((err) => {
        res.status(400).json(err);
    });
}

export function SaveAllPrices(req, res) {
    const options = {

        headers: {'X-CoinAPI-Key': 'CE923B9F-B8E2-4036-871B-0902E15CEB30'},
        method: 'get',
        url: 'https://rest.coinapi.io/v1/trades/LIVECOIN_SPOT_ARK_USD/latest?limit=10000',
        responseType: 'json'
    };
    axios(options).then((response) => {
        Price.remove({}, function(err) {
            if (err) {
                console.log(err)
            } else {
                Price.collection.insert(response.data, function(err, docs) {
                    if (err) {
                        console.log('err', err);
                    } else {
                        console.log('successfully stored.', docs.length);
                        res.status(200).json({success: true});
                    
                    }
                });
            }
        });
  
     
    })
    .catch((err) => {
        console.log('err', err);
        res.status(400).json(err);
    });
}