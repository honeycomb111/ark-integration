'use strict';
require('../models/price');


const express = require('express');
const axios = require('axios');
var mongoose = require('mongoose'), 
    Price = mongoose.model('price');
var CronJob = require('cron').CronJob;

module.exports = {
createCroneJob: function () {
	
    new CronJob('0 */15 * * * *', function() {
            const options = {

                headers: {'X-CoinAPI-Key': '25DC9A66-FA0D-44FB-9C25-82F2E78F8938'},
                method: 'get',
                url: 'https://rest.coinapi.io/v1/trades/LIVECOIN_SPOT_ARK_USD/latest?limit=10000',
                responseType: 'json'
            };
            axios(options).then((response) => {
                Price.remove({}, function(err) {
                    if (err) {
                        console.log(err)
                    } else {
                        Price.collection.insert(response.data, function(err, docs) {
                            if (err) {
                                console.log('err', err);
                            } else {
                                console.log('successfully stored.', docs.length);
                                
                            
                            }
                        });
                    }
                });
        
            
            })
            .catch((err) => {
                console.log('err', err);
            
            });
        }, null, true, 'America/Los_Angeles');
    }
};