import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../Home/blockActions';


class Contact extends Component {
    constructor (props) {
        super(props);
        this.state = {name: '', address: '', description: '' , isSend: false};
    }
    sendMail =  () => {
        const {name, address, description, isSend} = this.state;
        let propObj = {
            name,
            address,
            description
        };
        this.setState({isSend: true, name: '', address: '', description: '' });
        this.props.sendContactMail(propObj);
    }
    render() {
        const {isSend} = this.state;
        return (
            <div className="section-1">
                <div className="container">

                    <ol className="breadcrumb custom-breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">Contact</li>
                    </ol>

                    <div className="faucet-main-box contact-box"> 
                            
                            <h4>How can we help?</h4>
                            <p>We're committed to finding the answers you need as quickly as<span className="block"></span> possible. Please tell us a little about what you need help with.</p>


                            <div className="row mt30">
                                
                                <div className="col-md-6 col-sm-8">
                                    
                                    <form className="form-horizontal" action="">
                                    <div className="form-group">
                                        <label className="control-label col-sm-4" htmlFor="">Nick Name:</label>
                                        <div className="col-sm-8">
                                        <input type="text" className="form-control" id="" placeholder="Enter Your Nick Name"    onChange={ (e) => {this.setState({name:e.target.value})} } value={this.state.name} />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label col-sm-4" htmlFor="">Address (Payment):</label>
                                        <div className="col-sm-8"> 
                                        <input type="text" className="form-control" id="" placeholder="Enter Your Payment Address" onChange={ (e) => {this.setState({address:e.target.value})} } value={this.state.address} />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-sm-4" htmlFor="">Your Message:</label>
                                        <div className="col-sm-8"> 
                                        <textarea className="form-control" id="" rows="7" placeholder="Type your question or a description of the problem you're trying to solve here (minimum of 10 characters)." onChange={ (e) => {this.setState({description:e.target.value})} } value={this.state.description}></textarea>
                                        </div>
                                    </div>

                                    <div className="form-group"> 
                                        <div className="col-sm-offset-4 col-sm-10">
                                            <button type="button" className="btn btn-send" onClick={() => this.sendMail()}>Send</button>
                                        </div>
                                    </div>
                                    {
                                        isSend ?
                                        <p className="alert-success">The message has been sent.</p>
                                        :
                                        null
                                    }
                                   
                                    </form>

                                </div> 

                            </div> 


                    </div> 
                        

                </div> 
            </div> 


        )
    }
}


const mapDispatchToProps = (dispatch) => {
      
    return {
     
        sendContactMail: (postObj) => { dispatch(actions.sendContactMail(postObj));}
      }
  }
  
  
  export default connect(null,mapDispatchToProps)(Contact);