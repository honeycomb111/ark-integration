import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import $ from 'jquery';

class SmudiHeader extends Component {
    state = { activeClass: 'home'};


    constructor(props) {
        super(props);
        
    }



    render() {
        const { activeClass } = this.state;

        return (
            <div>
                <header>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 logo">
                                <img src={require('../../img/2313716.gif')} />
                            </div>
                        </div>
                    </div>
                </header>

                <nav className="navbar navbar-default">
                    <div className="container">
                        <ul className="nav navbar-nav">
                        <li className={activeClass == 'home' ? 'active' : ''}><Link to="/home" onClick={() => this.setState({activeClass: 'home'})}><i className="fa fa-home"></i>Home</Link></li>
                        <li className={activeClass == 'calculator' ? 'active' : ''}><Link to="/calculator" onClick={() => this.setState({activeClass: 'calculator'})}><i className="fa fa-calculator"></i>Calculator</Link></li>
                        <li className={activeClass == 'payment' ? 'active' : ''}><Link to="/payment" onClick={() => this.setState({activeClass: 'payment'})}><i className="fa fa-credit-card"></i>Payments</Link></li>
                        <li className={activeClass == 'news' ? 'active' : ''}><a  target="_blank" href="https://medium.com/@Delegate_Del/latest" onClick={() => this.setState({activeClass: 'news'})}><i className="fa fa-home"></i>News</a></li>
                        <li className={activeClass == 'faucet' ? 'active' : ''}><Link to="/faucet" onClick={() => this.setState({activeClass: 'faucet'})}><i className="fa fa-home"></i>Faucet</Link></li>
                        <li className={activeClass == 'faq' ? 'active' : ''}><Link to="/faq" onClick={() => this.setState({activeClass: 'faq'})}><i className="fa fa-calculator"></i>FAQ</Link></li>
                        <li className={activeClass == 'contact' ? 'active' : ''}><Link to="/contact" onClick={() => this.setState({activeClass: 'contact'})}><i className="fa fa-envelope"></i>Contact</Link></li>
                        </ul>
                    </div>
                </nav>
                
            </div>
        )
    }

}



export default SmudiHeader;