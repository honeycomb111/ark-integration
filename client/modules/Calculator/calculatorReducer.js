import { LOAD_CALCULATIONS } from './actionTypes';

export default function( state={}, action) {
    switch (action.type) {

        case LOAD_CALCULATIONS:
        
            return { ...state, dailyProfit: action.payload.dailyProfit,weeklyProfit: action.payload.weeklyProfit, monthlyProfit: action.payload.monthlyProfit}

  
            
    }
    return state;

}