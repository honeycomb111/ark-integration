import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../Home/blockActions';
import Linkify from 'react-linkify';



class Calculator extends Component {
    constructor(props) {
        super(props);
        this.state = {amount : ''};
    }
    componentDidMount() {
    
        this.props.getAllDetails();
        this.props.getAllTweets();
    
        
    }
    render() {
        const {calculate} = this.props;
        return (
            <div className="section-1">
                <div className="container">

                    <ol className="breadcrumb custom-breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">Calculator</li>
                    </ol>

                    <div className="row">
                        <div className="col-md-7">
                            <div className="panel-group trade-wrapper">
                            <div className="panel panel-default">
                            <div className="panel-heading">Profit Calculator</div>
                            <div className="panel-body">
                                    
                                <div className="gray-box">
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12">
                                        <div className="form-group">
                                            <input type="text"  className="form-control" onChange={ (e) => {this.setState({amount: e.target.value})} } value={this.state.amount} placeholder="Ark Amount" name="" />
                                        </div>
                                        </div>
                                        <div className="col-sm-6 col-md-6">
                                        {/* <div className="form-group">
                                            <input type="text"  className="form-control" placeholder="Ark Address" name="" />
                                        </div> */}
                                        </div>
                                    </div> 
                                    <div  className="row">
                                        <div className="col-sm-6 col-md-6 text-center">
                                            <span><a onClick={() => this.setState({amount: ''}, () => this.props.calculateProfit(0))} style={{cursor:'pointer'}} className="resettext">Reset</a></span> <a onClick={() => this.props.calculateProfit(this.state.amount)} className="btn btn-calculate-profile">Calculate profit</a>
                                        </div>
                                    </div> 

                                </div> 

                                <div className="panel panel-default custom-panel">
                                    <div className="panel-heading">PROFIT CALCULATOR RESULTS</div>
                                    <div className="panel-body">
                                        
                                        <div className="row calculator-result text-center">
                                            <div className="col-md-4 col-sm-4">
                                                <h4>Daily Profit</h4>
                                                <p>{(calculate && calculate.dailyProfit)? `${calculate.dailyProfit} ARK` : '0 ARK'} </p>
                                            </div>
                                            <div className="col-md-4 col-sm-4">
                                                <h4>Weekly</h4>
                                                <p>{(calculate && calculate.weeklyProfit)? `${calculate.weeklyProfit} ARK` : '0 ARK'}  </p>
                                            </div>
                                            <div className="col-md-4 col-sm-4">
                                                <h4>Monthly</h4>
                                                <p>{(calculate && calculate.monthlyProfit)? `${calculate.monthlyProfit} ARK` : '0 ARK'}  </p>
                                            </div>
                                        </div>

                                    </div>
                                </div> 


                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-5">
                            <div className="panel-group tweets-wrapper">
                                <div className="panel panel-default">
                                    <div className="panel-heading">Tweets</div>
                                    <div className="panel-body" style={{maxHeight:'500px',overflowY:'auto'}}>
                                        <ul>
                                        {this.props.tweets.map((tweet,i) => {
                                            return (
                                                <Linkify key={i}>
                                                <li>
                                                  <div className="img-wrpper" style={{marginBottom:'5px'}}>
                                                    <img className="img-responsive" src={tweet.user.profile_background_image_url_https} style={{display:'inline-block',width: '32px', height:'32px',marginRight:'5px', marginBottom:'5px'}}/>
                                                    <span className="info-1" style={{marginRight:'5px'}}>{tweet.user.name}</span><span style={{color:'#585858'}}>@{tweet.user.screen_name}</span>
                                                  </div>
                                                  <div className="info-wrapper">
                                                  
                                                    <p className="info-2"><span>{tweet.text}</span></p>
                                                    <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                                  </div>
                                          
                                                </li>
                                              </Linkify>
                                            )
                                            })}
                                        </ul>
                                    </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
function mapStateToProps(state) {
    
    return { 
  
      blockDetails: state.blockDetails,
      calculate: state.calculate,
      tweets: state.tweets
    };
  }
  
  
  const mapDispatchToProps = (dispatch) => {
        
    return {
     
        getAllDetails: () => { dispatch(actions.getAllDetails());},
        calculateProfit: (amount) => { dispatch(actions.calculateProfit(amount));},
        getAllTweets: () => { dispatch(actions.getAllTweets());}
      }
  }
  
  
export default connect(mapStateToProps,mapDispatchToProps)(Calculator);