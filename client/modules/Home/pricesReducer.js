import { LOAD_ALL_PRICES } from './actionTypes';

export default function ( state = [], action) {
    
    
    switch ( action.type ) {
        
        case LOAD_ALL_PRICES:

            state= [];
            return [ ...state, ...action.payload ];
        
    }
    
    return state;
    
};