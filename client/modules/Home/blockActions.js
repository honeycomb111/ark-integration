import axios from 'axios';
import Config from '../../../server/config';
import { LOAD_BLOCK_DETAILS, LOAD_ALL_VOTERS, UPDATE_BLOCK_DETAILS, LOAD_ALL_PRICES } from './actionTypes';
import { LOAD_ALL_TWEETS } from '../Payment/actionTypes';
import { LOAD_CALCULATIONS } from '../Calculator/actionTypes';
import _ from 'lodash';

export const API_URL = (typeof window === 'undefined' || process.env.NODE_ENV === 'test') ?
process.env.BASE_URL || (`http://localhost:${process.env.PORT || Config.port}/api`) :
'/api';


export function getAllDetails() {
    return function(dispatch) {
        
        axios.get(`${API_URL}/payments/getAllPayments`)
        .then(response => {
            
            dispatch({ type: LOAD_BLOCK_DETAILS, payload: response.data.delegate});
            axios.get(`${API_URL}/payments/getAllVoters`)
            .then(votersResponse => {
                votersResponse.data.accounts.forEach(element => {
                    
                    //let vote = (response.data.delegate.vote / 100000000).toFixed(0);
                    let voterWeight = (element.balance / response.data.delegate.vote ) * 100;
                    element.voterWeight = voterWeight.toFixed(4);
             
                    element.status = "Active";

                });
                votersResponse.data.accounts = _.orderBy(votersResponse.data.accounts,['voterWeight'],['desc']);
                dispatch({ type: LOAD_ALL_VOTERS, payload: votersResponse.data.accounts});
            })
            axios.get(`${API_URL}/payments/getBlockHeight`)
            .then(response => {
                
                dispatch({ type: UPDATE_BLOCK_DETAILS, payload: response.data.height});
            })
        })
        .catch(err => console.log('error',err));
    }
}

export function getAllVoters() {
    return function(dispatch) {
        
        axios.get(`${API_URL}/payments/getAllVoters`)
        .then(response => {
            
            dispatch({ type: LOAD_ALL_VOTERS, payload: response.data.accounts});
        })
        .catch(err => console.log('error',err));
    }
}

export function getAllTweets() {
    return function(dispatch) {
        
        axios.get(`${API_URL}/payments/getAllTweets`)
        .then(response => {
            
            dispatch({ type: LOAD_ALL_TWEETS, payload: response.data.tweets});
        })
        .catch(err => console.log('error',err));
    }
}

export function calculateProfit(amount) {
    return function(dispatch, getState) {
        const {delegate} = getState().blockDetails;
    
        const dailyArkAmount = 422;
        if (delegate) {
            let poolSize = delegate.vote;
            amount = amount * 100000000;
            let dailyProfit = ((amount / poolSize) *  dailyArkAmount * 0.95) / 2.24;
            dailyProfit = parseFloat(dailyProfit.toFixed(6));
            let weeklyProfit = dailyProfit * 7;
            weeklyProfit = parseFloat(weeklyProfit.toFixed(6));
            let monthlyProfit = dailyProfit * 30;
            monthlyProfit = parseFloat(monthlyProfit.toFixed(6));
            dispatch({ type: LOAD_CALCULATIONS, payload: {dailyProfit,weeklyProfit, monthlyProfit }});
        }
        
    }

}

export function getBlockHeight() {
    return function(dispatch) {
        
        axios.get(`${API_URL}/payments/getBlockHeight`)
        .then(response => {
            
            dispatch({ type: UPDATE_BLOCK_DETAILS, payload: response.data.height});
        })
        .catch(err => console.log('error',err));
    }
}

export function sendContactMail(propsObject) {
    return function(dispatch) {
  
        axios.post(`${API_URL}/payments/sendMailContacts`, propsObject)
        .then(response => {
       
      
        })
        .catch(err => console.log('error',err));
    }
}

export function getAllPrices () {
    return function (dispatch) {
        axios.get(`${API_URL}/payments/getAllPrices`)
        .then(response => {
            dispatch({ type: LOAD_ALL_PRICES, payload: response.data });
        })
        .catch(err => console.log('error',err));
    }
}
export function saveAllPrices () {
    return function (dispatch) {
        axios.post(`${API_URL}/payments/saveAllPrices`)
        .then(response => {
      
        })
        .catch(err => console.log('error',err));
    }
}