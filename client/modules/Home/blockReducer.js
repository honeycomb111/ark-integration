import { LOAD_BLOCK_DETAILS,UPDATE_BLOCK_DETAILS } from './actionTypes';

export default function( state={}, action) {
    switch (action.type) {

        case LOAD_BLOCK_DETAILS:
            return { delegate: action.payload };
        
        case UPDATE_BLOCK_DETAILS:
            return { ...state, height: action.payload}

  
            
    }
    return state;

}