import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import $ from 'jquery';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Linkify from 'react-linkify';
import Chart from '../Chart/chart'

import * as actions from './blockActions';
import * as paymentActions from '../Payment/paymentActions';


class Home extends Component {
  
  constructor(props) {
    super(props);
    this.state = { timeInterval: '1year' };

  }
  componentDidMount() {
      this.props.getAllDetails();
    //  this.props.getAllVoters();
      this.props.getAllTweets();
    
      this.props.getAllTransactions();
      this.props.getAllPrices();
  
  }

  render() {
    const {delegate, height} = this.props.blockDetails;
    const {voters} = this.props;
   

    return (
      <div>
        <div className="box1-wrapper">
          <div className="container">
            <div className="well"><marquee style={{paddingBottom:'5px', paddingTop:'5px'}}>Welcome to <span>Delegate Del</span>’s webpage</marquee></div>
          </div>
        </div>

        <div className="box2-wrapper">
          <div className="container">
            <div className="well">
              <ul>
                <li>Total votes: <span>{delegate ? (delegate.vote / 100000000).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") : 0} Votes</span> </li>
                <li>Blocks forged: <span>{delegate ? delegate.producedblocks : 0} Total</span> </li>
                <li>Missed blocks: <span>{delegate ? delegate.missedblocks : 0} Total</span> </li>
                <li>Productivity: <span>{delegate ? delegate.productivity: 0} Rank</span> </li>
                {/* <li>Status <span>Forging</span> </li> */}
              </ul>
            </div>
          </div>
        </div>
        <div className="section-1">
          <div className="container">
              <div className="row">
                <div className="col-md-7">
                    <div className="panel-group trade-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Trade Graph</div>
                      <div className="panel-body">
                          <h3>ARK price</h3>
                          {/* <img className="img-responsive" src={require('../../img/graph.png')}/> */}
                          <Chart timeInterval={this.state.timeInterval} />
                          <div className="col-md-12 col-sm-12" style={{marginTop:'10px'}}>
                         
                              <a className={`btn btn-calculate-profile ${this.state.timeInterval === '24hours' ? 'isActive': null}`} style={{borderRadius: '22px', marginRight:'10px', padding:'8px 15px'}} onClick={() => this.setState({timeInterval:'24hours'})}>24 Hours</a>
                       
                              <a className={`btn btn-calculate-profile ${this.state.timeInterval === '7days' ? 'isActive': null}`}  style={{borderRadius: '22px', marginRight:'10px', padding:'8px 15px'}} onClick={() => this.setState({timeInterval:'7days'})}>7 Days</a>
                            
                              <a className={`btn btn-calculate-profile ${this.state.timeInterval === '1month' ? 'isActive': null}`}  style={{borderRadius: '22px', marginRight:'10px', padding:'8px 15px'}}  onClick={() => this.setState({timeInterval:'1month'})}>1 Month</a>
                        
                 
                              <a className={`btn btn-calculate-profile ${this.state.timeInterval === '1year' ? 'isActive': null}`}  style={{borderRadius: '22px', marginRight:'10px', padding:'8px 15px'}} onClick={() => this.setState({timeInterval:'1year'})}>1 Year</a>
                          
                          
                          
                          </div>
                     
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-5">
                    <div className="panel-group tweets-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Tweets</div>
                      <div className="panel-body" style={{maxHeight:'500px',overflowY:'auto'}}>
                          <ul>
                            {this.props.tweets.map((tweet, i) => {
                              let myString = "" + tweet.text.toString();
                              return (
                                <Linkify key={i}>
                                  <li>
                                    <div className="img-wrpper" style={{marginBottom:'5px'}}>
                                      <img className="img-responsive" src={tweet.user.profile_background_image_url_https} style={{display:'inline-block',width: '32px', height:'32px',marginRight:'5px', marginBottom:'5px'}}/>
                                      <span className="info-1" style={{marginRight:'5px'}}>{tweet.user.name}</span><span style={{color:'#585858'}}>@{tweet.user.screen_name}</span>
                                    </div>
                                    <div className="info-wrapper">
                                    
                                      <p className="info-2"><span>{tweet.text}</span></p>
                                      <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                    </div>
                            
                                  </li>
                                </Linkify>
                              )
                            })}
                            {/* <li>
                                <div className="img-wrpper">
                                  <img className="img-responsive" src={require('../../img/tweets-img.png')} style={{width: '32px', height:'32px'}}/>
                                </div>
                                <div className="info-wrapper">
                                  <p className="info-1">PeezeFPF<span>@PeezeFPF</span></p>
                                  <p className="info-2">@zacquivi <span>that avatar</span> #fpf</p>
                                  <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                </div>
                                <div className="hours-wrapper">
                                  <p className="icon"><i className="fa fa-twitter"></i></p>
                                  <p className="hours">5h</p>
                                </div>
                            </li>
                            <li>
                                <div className="img-wrpper">
                                <img className="img-responsive" src={require('../../img/tweets-img.png')} style={{width: '32px', height:'32px'}}/>
                                </div>
                                <div className="info-wrapper">
                                  <p className="info-1">PeezeFPF<span>@PeezeFPF</span></p>
                                  <p className="info-2">@zacquivi <span>that avatar</span> #fpf</p>
                                  <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                </div>
                                <div className="hours-wrapper">
                                  <p className="icon"><i className="fa fa-twitter"></i></p>
                                  <p className="hours">5h</p>
                                </div>
                            </li>
                            <li>
                                <div className="img-wrpper">
                                <img className="img-responsive" src={require('../../img/tweets-img.png')} style={{width: '32px', height:'32px'}}/>
                                </div>
                                <div className="info-wrapper">
                                  <p className="info-1">PeezeFPF<span>@PeezeFPF</span></p>
                                  <p className="info-2">@zacquivi <span>that avatar</span> #fpf</p>
                                  <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                </div>
                                <div className="hours-wrapper">
                                  <p className="icon"><i className="fa fa-twitter"></i></p>
                                  <p className="hours">5h</p>
                                </div>
                            </li>
                            <li>
                                <div className="img-wrpper">
                                <img className="img-responsive" src={require('../../img/tweets-img.png')} style={{width: '32px', height:'32px'}}/>
                                </div>
                                <div className="info-wrapper">
                                  <p className="info-1">PeezeFPF<span>@PeezeFPF</span></p>
                                  <p className="info-2">@zacquivi <span>that avatar</span> #fpf</p>
                                  <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                </div>
                                <div className="hours-wrapper">
                                  <p className="icon"><i className="fa fa-twitter"></i></p>
                                  <p className="hours">5h</p>
                                </div>
                            </li>
                            <li>
                                <div className="img-wrpper">
                                <img className="img-responsive" src={require('../../img/tweets-img.png')} style={{width: '32px', height:'32px'}}/>
                                </div>
                                <div className="info-wrapper">
                                  <p className="info-1">PeezeFPF<span>@PeezeFPF</span></p>
                                  <p className="info-2">@zacquivi <span>that avatar</span> #fpf</p>
                                  <p className="info-icons"><i className="fa fa-heart"></i><i className="fa fa-sign-out"></i></p>
                                </div>
                                <div className="hours-wrapper">
                                  <p className="icon"><i className="fa fa-twitter"></i></p>
                                  <p className="hours">5h</p>
                                </div>
                            </li> */}
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div className="section-2">
          <div className="container">
              <div className="row">
                <div className="col-md-4 block-1">
                    <div className="panel-group delegate-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Delegate</div>
                      <div className="panel-body">
                          <div className="left">
                            <p className="sub" style={{marginRight:'5px'}}>Rank:  </p>
                            <p className="main"> 35</p>
                          </div>
                          <div className="right">
                          <p className="sub"style={{marginRight:'5px'}}>Voters:</p>
                            <p className="main">{voters.length}</p>
                     
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 block-2">
                    <div className="panel-group node-stats-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Node Stats</div>
                      <div className="panel-body">
                          <div className="left">
                            <p className="main">Forging</p>
                            <p className="sub">Status</p>
                          </div>
                          <div className="right">
                            <p className="main">{delegate ? `${delegate.productivity}%`: 0} </p>
                            <p className="sub">Productivity:</p>
                        
                            
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 block-3">
                    <div className="panel-group blockchain-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Blockchain</div>
                      <div className="panel-body">
                          <div className="left">
                            <p className="main">{height? height: 0} </p>
                            <p className="sub">Block Height</p>
                          </div>
                          <div className="right">
                            <p className="main">{delegate ? delegate.producedblocks: 0} </p>
                            <p className="sub">Mined Blocks</p>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div className="section-3">
          <div className="container">
              <div className="row">
                <div className="col-md-8">
                    <div className="table-wrapper">
                      <h2>Voters</h2>    
                      { voters ? 
                          <BootstrapTable
                            ref='table'
                            data={voters}
                            striped hover condensed pagination
                          >
                              <TableHeaderColumn  tdStyle={ { 'width': '60%' } } dataAlign='center' thStyle={ { 'width': '60%' } } width='60%' dataField="address" >Address</TableHeaderColumn>
                            <TableHeaderColumn width='20%' tdStyle={ { 'width': '20%' } } dataAlign='center' thStyle={ { 'width': '20%' } } dataField="status" >Status</TableHeaderColumn>
                          
                            <TableHeaderColumn  width='20%' tdStyle={ { 'width': '20%' } } dataAlign='center' thStyle={ { 'width': '20%' } } dataField="voterWeight">Vote Weight</TableHeaderColumn>
                            <TableHeaderColumn dataField="publicKey" isKey hidden>Public kEY</TableHeaderColumn>
               
                          </BootstrapTable>
                        :
                        null
                      }

                      {/* <table className="table datatable">
                        <thead>
                          <tr>
                            <th width="400">Address</th>
                            <th>Status</th>
                            <th className="text-center">Vote Weight</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table> */}
                    </div>
                 
                </div>
                <div className="col-md-4">
                    <div className="panel-group notification-wrapper">
                    <div className="panel panel-default">
                      <div className="panel-heading">Notification Feed</div>
                      <div className="panel-body">
                          <ul>
                            { this.props.transactions.map((transaction,i)=> {
                              if (i < 4) {
                                  return (
                                    <li key={i}>
                                      <div className="icon-wrapper">
                                        <span className="circle">
                                            <i className="fa fa-dollar"></i>
                                        </span>
                                      </div>
                                      <div className="status-wrapper">
                                        <h5>Payments have been sent</h5>
                                        <p className="text">Total {transaction.amount} Ark  were sent. </p>
                                        <p className="time">{transaction.timestamp}</p>
                                      </div>
                                  </li>
                                  )
                            }})
                            }
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
         
      )
  }
}


function mapStateToProps(state) {
  
  return { 

    blockDetails: state.blockDetails,
    voters: state.voters,
    tweets: state.tweets,
    transactions: state.transactions
  };
}


const mapDispatchToProps = (dispatch) => {
      
  return {
   
      getAllDetails: () => { dispatch(actions.getAllDetails());},
  
      getAllTweets: () => { dispatch(actions.getAllTweets());},
      getBlockHeight: () => { dispatch(actions.getBlockHeight());},
      getAllTransactions: () => { dispatch(paymentActions.getAllTransactions());},
      getAllPrices: () => { dispatch(actions.getAllPrices());},
      saveAllPrices: () => { dispatch(actions.saveAllPrices());}

    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Home);