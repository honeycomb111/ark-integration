import { LOAD_ALL_VOTERS } from './actionTypes';

export default function ( state = [], action) {
    
    
    switch ( action.type ) {
        
        case LOAD_ALL_VOTERS:

            state= [];
            return [ ...state, ...action.payload ];
        
    }
    
    return state;
    
};