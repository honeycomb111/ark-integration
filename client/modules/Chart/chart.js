import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';
import _ from 'lodash';
var array = require('./file');
const moment = require('moment');
let props;





class Chart extends Component {
    constructor(props) {
        super(props);
        this.selectData = this.selectData.bind(this);
        this.selectLabels = this.selectLabels.bind(this);
        this.isToday = this.isToday.bind(this);
        this.isYesterday = this.isYesterday.bind(this);
        this.isWithinAWeek = this.isWithinAWeek.bind(this);
        this.isWithinMonth = this.isWithinMonth.bind(this);
    }
    
  
    selectData = function() {

    const {timeInterval, prices} = this.props;
  
    if (this.props.timeInterval == '24hours') {
  
      if (this.props.prices && this.props.prices.length > 0) {
        var newArray = this.props.prices;

        let priceCollection = [];
        newArray.forEach((item) => {
          debugger;
          if (this.isToday(moment(item.time_exchange)) || this.isYesterday(moment(item.time_exchange))) {
            priceCollection.push(item.price);
          }
          
        })
        return priceCollection;
      }
    }
      else if (timeInterval === '7days') {
        if (this.props.prices && this.props.prices.length > 0) {
          var newArray = this.props.prices;
          let priceCollection = [];
          newArray.forEach((item) => {
            if (this.isWithinAWeek(moment(item.time_exchange))) {
              priceCollection.push(item.price);
            }
          })
          return priceCollection;

        }

      }
      else if (timeInterval === '1month') {
      
        if (this.props.prices && this.props.prices.length > 0) {
          var newArray = this.props.prices;
          let priceCollection = [];
          newArray.forEach((item)  =>{
            if (this.isWithinMonth(moment(item.time_exchange))) {
              priceCollection.push(item.price);
            }
           
          })
          return priceCollection;
        }
          
     
      }
      else if (timeInterval === '1year' || timeInterval === '5years') {
        
        var newArray = this.props.prices;
          
        let priceCollection = [];
        newArray.forEach(function(item) {
          priceCollection.push(item.price);
        })
        return priceCollection;
      }

    }
 
    
    selectLabels = function() {

      const {timeInterval} = this.props;
      if (timeInterval === '24hours') {
        if (this.props.prices && this.props.prices.length > 0) {
        var newArray = this.props.prices;
        let dateCollection = [];
      
        newArray.forEach((item) => {
        
          if (this.isToday(moment(item.time_exchange)) || this.isYesterday(moment(item.time_exchange))) {
            debugger;
            dateCollection.push(item.time_exchange);
          }
        
        })
        return dateCollection;
        }
        
      }
      else if (timeInterval === '7days') {
      
    
        if (this.props.prices && this.props.prices.length > 0) {
          var newArray = this.props.prices;
          
          let dateCollection = [];
          newArray.forEach(item =>{
            if (this.isWithinAWeek(moment(item.time_exchange))) {
              dateCollection.push(item.time_exchange);
            }
         
          })
          return dateCollection;
        }
      }
      else if (timeInterval === '1month') {
        
        if (this.props.prices && this.props.prices.length > 0) {
          var newArray = this.props.prices;
          let dateCollection = [];
          newArray.forEach((item) => {
            if (this.isWithinMonth(moment(item.time_exchange))) {
              dateCollection.push(item.time_exchange);
            }
          })
          return dateCollection;
        }
      }
      else if (timeInterval === '1year' || timeInterval === '5years') {
    
        var newArray = this.props.prices;
        let dateCollection = [];
        let date = moment();
        debugger;
        newArray.forEach(function(item) {
   
          dateCollection.push(item.time_exchange);
        })
        return dateCollection;
      }

    }
    isToday(momentDate) {
      var REFERENCE = moment("2018-04-05"); 
      var TODAY = REFERENCE.clone().startOf('day')
      return momentDate.isSame(TODAY, 'd');
    }
    isYesterday(momentDate) {
      var REFERENCE = moment("2018-04-05"); 
      var YESTERDAY = REFERENCE.clone().subtract(1, 'days').startOf('day');
      return momentDate.isSame(YESTERDAY, 'd');
    }
    isWithinAWeek(momentDate) {
      var REFERENCE = moment(); 
      var A_WEEK_OLD = REFERENCE.clone().subtract(7, 'days').startOf('day');
      return momentDate.isAfter(A_WEEK_OLD);
    }
    isTwoWeeksOrMore(momentDate) {
      return !isWithinAWeek(momentDate);
    }
    isWithinMonth(momentDate) {
      var REFERENCE = moment(); 
      var A_WEEK_OLD = REFERENCE.clone().subtract(30, 'days').startOf('day');
      return momentDate.isAfter(A_WEEK_OLD);
    }
    render() {
        const options = {
          legend: {
            display: false,
          },
          scales: {
            xAxes: [{
              gridLines: {
                display: false
              },
              unit: "day",
              unitStepSize: 1000,
              type: 'time',
              time: {
                  displayFormats: {
                      millisecond: 'DD MMM HH:mm',
                      second: 'DD MMM HH:mm',
                      minute: 'DD MMM HH:mm',
                      hour: 'DD MMM HH:mm',
                      day: 'DD MMM HH:mm',
                      week: 'DD MMM HH:mm',
                      month: 'DD MMM HH:mm',
                      quarter: 'DD MMM HH:mm',
                      year: 'DD MMM HH:mm',
                  }
              }
            }],
            yAxes: [{
              position: 'right',
              ticks: {
            
                fontStyle: "bold",
                beginAtZero: true,
                callback: function(value, index, values) {
                  if(parseInt(value) >= 1000){
                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  } else {
                    return '$' + value;
                  }
                }
              }
            }]
          }
        }
        const data = (canvas) => {
          const ctx = canvas.getContext("2d")
          
          const gradient = ctx.createLinearGradient(0,0,100,0);
          var gradientFill = ctx.createLinearGradient(500, 0, 400, 200);
         
          gradientFill.addColorStop(0, "rgba(71, 237, 223, 0.796)");
          gradientFill.addColorStop(1, "#e3e3e3");
            return {
              labels: this.selectLabels(),
              datasets: [
                {
                  label: false,
                  fill: true,
                  lineTension: 0,
                  backgroundColor: gradientFill,                      //  'rgba(71, 237, 223, 0.796)'
                  borderColor: '#1592A3',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: '#1592A3',
                  pointBackgroundColor: '#ccc',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: '#1592A3',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: this.selectData(),
                },
              ],
            }
         
        }
        
        return (
            <div>
                <Line data={data} options={options} />
            </div>
        )
    }
}

function mapStateToProps(state) {
  
  return { 

    prices: state.prices
  };
}


export default connect(mapStateToProps,null)(Chart);