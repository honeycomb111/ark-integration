import { LOAD_ALL_TWEETS } from './actionTypes';

export default function ( state = [], action) {
    
    
    switch ( action.type ) {
        
        case LOAD_ALL_TWEETS:

            state= [];
            return [ ...state, ...action.payload ];
        
    }
    
    return state;
    
};