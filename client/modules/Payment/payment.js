import React, {Component} from 'react';
import { connect } from 'react-redux';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import * as actions from './paymentActions';



class Payment extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.getAllTransactions();
    }
    render() {

        const tableOptions = {
    
            defaultSortName: 'timestamp',
            defaultSortOrder: 'asc',
        };

        return (
            <div className="section-1">
                <div className="container">

                    <ol className="breadcrumb custom-breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">Payment Detail</li>
                    </ol>

                    <div className="payment-page">
                            <div className="row">
                                <div className="col-md-8">
                                    <div className="latest-payment">
                                        <h4>Latest Payments</h4><br />
                                        {this.props.transactions ? 
                                            <BootstrapTable
                                               ref='table'
                                               data={this.props.transactions}
                                               options={tableOptions}
                                               striped hover condensed pagination
                                            >
                                             
                                               <TableHeaderColumn dataField="amount" tdStyle={ { 'width': '33.33%' } } width='33.33%' thStyle={ { 'width': '33.33%' } } dataSort={true}>Paid Amount</TableHeaderColumn>
                                               <TableHeaderColumn dataField="timestamp" tdStyle={ { 'width': '33.33%' } } width='33.33%' thStyle={ { 'width': '33.33%' } } dataSort={true}>Time Sent</TableHeaderColumn>
                                               <TableHeaderColumn dataField="blockid" tdStyle={ { 'width': '33.33%' } } width='33.33%' thStyle={ { 'width': '33.33%' } }>Block Id</TableHeaderColumn>
                                               <TableHeaderColumn dataField="id" isKey hidden>Transaction</TableHeaderColumn>
                                    
                                            </BootstrapTable>
                                            :
                                            null
                                        }
                                     
                                        {/* <table id="example" className="table table-striped table-bordered nowrap" cellSpacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Address</th>
                                            <th>Paid Amount</th>
                                            <th>Time Sent</th>
                                            <th>Transection</th>

                                        </tr>
                                        </thead>
                                    
                                        <tbody>
                                        <tr>
                                            <td>3049 Old Dear Lane<span className="block"></span> Milton NY 12547</td>
                                            <td className="green">20 USD</td>
                                            <td>12:08 PM EST</td>
                                            <td><a className="View-btn" href=""><span className="fa fa-share-alt-square"></span> &nbsp;View on Explorer</a></td>

                                        </tr>
                                        
                                        <tr>
                                            <td>3049 Old Dear Lane<span className="block"></span> Milton NY 12547</td>
                                            <td className="green">20 USD</td>
                                            <td>12:08 PM EST</td>
                                            <td><a className="View-btn" href=""><span className="fa fa-share-alt-square"></span> &nbsp;View on Explorer</a></td>

                                        </tr>
                                        <tr>
                                            <td>3049 Old Dear Lane<span className="block"></span> Milton NY 12547</td>
                                            <td className="green">20 USD</td>
                                            <td>12:08 PM EST</td>
                                            <td><a className="View-btn" href=""><span className="fa fa-share-alt-square"></span> &nbsp;View on Explorer</a></td>

                                        </tr>

                                        
                                        
                                        </tbody>


                                    </table> */}

                                    </div> 
                                </div>
                                <div className="col-md-4">
                                    <div className="notificaiton-feed">
                                        <h4>Notification Feed</h4><br />
                                        { this.props.transactions.map((transaction,i)=> {
                                            if (i < 3) {
                                                return (
                                                    <div key={i} className="media mymedia">
                                                        <div className="media-left">
                                                            <a href="#">
                                                                <span className="fa fa-dollar"></span>
                                                            </a>
                                                        </div>
                                                        <div className="media-body">
                                                            <h5 className="media-heading">Payments have been sent</h5>
                                                            <p>Total {transaction.amount} Ark  were sent <span className="block"></span> {transaction.timestamp}</p>
                                                                
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        
                                        })

                                        }
                                  
                                    </div>
                                </div>
                            </div>


                    </div> 
                        

                </div> 
            </div> 
        )
    }
}


function mapStateToProps(state) {
    
    return { 
  
        transactions: state.transactions
    };
  }
  
  
  const mapDispatchToProps = (dispatch) => {
        
    return {
     
        getAllTransactions: () => { dispatch(actions.getAllTransactions());}
      }
  }
  
  
  export default connect(mapStateToProps,mapDispatchToProps)(Payment);


