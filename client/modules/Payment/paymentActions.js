import axios from 'axios';
import Config from '../../../server/config';
import { LOAD_ALL_TRANSACTIONS } from './actionTypes';
const _ = require('lodash');

const moment = require('moment');

export const API_URL = (typeof window === 'undefined' || process.env.NODE_ENV === 'test') ?
process.env.BASE_URL || (`http://localhost:${process.env.PORT || Config.port}/api`) :
'/api';


export function getAllTransactions() {
    return function(dispatch) {
        
        axios.get(`${API_URL}/payments/getAllTransactions`)
        .then(response => {
        
            response.data.transactions.forEach(element => {
            
                element.timestamp = moment(element.timestamp).format("h:mm:ss");
                element.amount = (element.amount / 100000000).toFixed(4);
                element.originalTimestamp = element.timestamp;
            });
            
            let orderTransactions = _.orderBy(response.data.transactions, ['timestamp'],['desc']);
            dispatch({ type: LOAD_ALL_TRANSACTIONS, payload: orderTransactions});
        })
        .catch(err => console.log('error',err));
    }
}