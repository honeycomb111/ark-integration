import { LOAD_ALL_TRANSACTIONS } from './actionTypes';

export default function ( state = [], action) {
    
    
    switch ( action.type ) {
        
        case LOAD_ALL_TRANSACTIONS:

            state= [];
            return [ ...state, ...action.payload ];
        
    }
    
    return state;
    
};