import React, { Component } from 'react';

class Faq extends Component {
    render() {
        return (
            <div className="">
                <div className="container">

                    <ol className="breadcrumb custom-breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">FAQ</li>
                    </ol>

                    <div className="faucet-main-box faq-box"> 
                            
                            <h4>FREQUENTLY ASKED QUESTIONS</h4><br />
                            
                            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    What is Delegated Proof-of-Stake (DPOS)?
                                </a>
                                </h4>

                                </div>
                                <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body">
                                        In an ordinary Proof of Work system, miners compete in order to solve a problem and receive a block reward. However in Ark, that issue is mitigated with <i>Delegated Proof of Stake (DPoS)</i> where users vote for individual <strong>delegates</strong> to be given that task. The task of <strong>forging</strong> a new block is assigned to one of the top 51 delegates with the most votes every 8 seconds. Adding a new block to the blockchain awards the forger with 2 newly minted Ark. This means every delegate gets an average of 422 Ark every 24 hours.
                                        <p>It is common practice for delegates to offer to share their forging rewards with their voters in order to entice them to continue voting for them and secure their spot in the top 51.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingTwo">
                                    <h4 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    How much do you share?
                                </a>
                                </h4>

                                </div>
                                <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div className="panel-body">By staking with <strong>Del</strong> you are guaranteed to receive a share of the <strong>95% of ALL are forged during a pay period.</strong> In order to determine how much ARK that you will be rewarded from staking, check out the calculator.<strong> Note:</strong> The amount of ARK displayed as reward is <strong>merely an estimation is not guaranteed, factors such as dilution of the pool due to high vote count can significantly influence the amount you are expecting to be paid.</strong> In other words, there are no guarantees in regards to how much you will get paid, but you will be guaranteed a payout from the 95% of the total forged ARK.</div>
                                </div>
                            </div>
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingThree">
                                    <h4 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How often does Del pay?
                                </a>
                                </h4>

                                </div>
                                <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div className="panel-body">The payouts are currently scheduled to be automatically paid out on a weekly basis. However, dynamic payout schedules are well underway.  </div>
                                </div>
                            </div>

                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingFour">
                                    <h4 className="panel-title">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            How do i vote?
                                        </a>
                                    </h4>

                                </div>
                                <div id="collapseFour" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">                                
                                    <div className="panel-body">If you are interested in supporting Del, you can learn how to vote by checking out the following [<a href="https://blog.ark.io/how-to-vote-or-un-vote-an-ark-delegate-and-how-does-it-all-work-819c5439da68">guide</a>]</div>
                                </div>
                            </div>


                        </div> 
                
                        <p className="fbold">For more FAQ <a  target="_blank" href="https://medium.com/@Delegate_Del/ark-questions-and-answers-e79e71a2574e" className="greentext">Click Here</a></p>
                        


                    </div> 
                        

                </div> 
            </div> 
        )
    }
}

export default Faq;