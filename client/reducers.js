/**
 * Root Reducer
 */
import { combineReducers } from 'redux';

// Import Reducers
import app from './modules/App/AppReducer';
import { reducer as form } from 'redux-form';
import blockReducer from './modules/Home/blockReducer';
import transactionsReducer from './modules/Payment/transactionsReducer';
import votersReducer from './modules/Home/votersReducer';
import tweetsReducer from './modules/Payment/tweetsReducer';
import calculatorReducer from './modules/Calculator/calculatorReducer';
import pricesReducer from './modules/Home/pricesReducer';



// Combine all reducers into one root reducer
export default combineReducers({
  app,
  blockDetails: blockReducer,
  transactions: transactionsReducer,
  voters: votersReducer,
  tweets: tweetsReducer,
  calculate: calculatorReducer,
  prices: pricesReducer,
  form
});
