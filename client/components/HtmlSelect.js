import React, { Component } from 'react';

const HtmlSelect = ({ name, items, value, displayText, onChange }) => {

    
    const renderSelectOptions = (item) => (
        <option key={item[value]} value={item[value]}>{item[displayText]}</option>
    )

    return (
        <div className="select_custom">
            <select name={name} onChange={onChange} className="form-control">
                <option value="">Select</option>
                {items.map(
                    function (item) { return renderSelectOptions(item); })}
            </select>
            <i className="fa fa-angle-down"></i>
        </div>
    );
}

export default HtmlSelect;